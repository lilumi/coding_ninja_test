<?php

add_action( 'wp_footer', 'lm_add_popup', 100);
function lm_add_popup() {
  get_template_part( 'lm-popup' );
}

add_action( 'wp_enqueue_scripts', 'lm_add_popup_js', 80 );
function lm_add_popup_js() {

  wp_enqueue_script( 'lm-popup', get_stylesheet_directory_uri().'/assets/js/lm-popup.js', array('jquery'), null, true );

}
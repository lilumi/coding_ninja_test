<div class="cf-overlay"></div>
<div class="cf-popup-wrapper" id="popup1">
  <div class="cf-popup">
    <a href="#" class="cf-close">×</a>
    <div class="cf-popup-content">
      <p>[Placeholder for contact form]</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur interdum ut metus ut volutpat. Pellentesque nec orci quam. Morbi rhoncus eget velit sed imperdiet. Sed lacinia tortor dignissim, tempor ipsum non, volutpat eros. Morbi accumsan, ligula vitae ullamcorper eleifend, ipsum dolor vulputate mi, quis maximus justo urna at mi. Nunc fermentum nisl et diam semper semper. In in viverra tellus, eget gravida sem. Sed tempus velit at justo mattis aliquet. Suspendisse potenti. Vestibulum sit amet fringilla eros.</p>
  
      <p>Fusce consectetur sem urna, nec elementum quam pharetra ac. In non lobortis diam. Fusce sollicitudin tempus aliquam. Aenean iaculis, turpis sit amet gravida tincidunt, arcu sapien sagittis velit, at placerat erat massa ac lectus. Suspendisse nec mattis magna. Praesent velit erat, placerat a sapien nec, ultricies iaculis nisl. Aliquam dictum, sapien sit amet porttitor placerat, nisl erat congue justo, sed placerat urna mi a turpis.</p>
  
      <p>Proin sagittis dui a leo luctus accumsan at et neque. Ut aliquet lectus ut faucibus convallis. Pellentesque lectus neque, cursus et luctus id, vehicula eget dolor. Sed porta, lectus non tincidunt congue, lacus ligula rutrum ex, quis tempor ante mauris id quam. Etiam id aliquam nisi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus eu lacus in tortor dictum commodo ut ut metus. Aliquam fringilla nunc non tortor ullamcorper condimentum.</p>
    </div>
  </div>
</div>
<script type="text/javascript">
(function($, window, document) {
  $(function() {

    $('.footer-cta .contact-form').cfPopup({
      'popup' : '#popup1'
    });   
  
    $('.site-info a').cfPopup({
      'popup' : '#popup2'
    }); 

  });
}(window.jQuery, window, document));

</script>

<div class="cf-popup-wrapper" id="popup2">
  <div class="cf-popup">
    <a href="#" class="cf-close">×</a>
    <div class="cf-popup-content">
      <p>[POPUP2]</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur interdum ut metus ut volutpat. Pellentesque nec orci quam. Morbi rhoncus eget velit sed imperdiet. Sed lacinia tortor dignissim, tempor ipsum non, volutpat eros. Morbi accumsan, ligula vitae ullamcorper eleifend, ipsum dolor vulputate mi, quis maximus justo urna at mi. Nunc fermentum nisl et diam semper semper. In in viverra tellus, eget gravida sem. Sed tempus velit at justo mattis aliquet. Suspendisse potenti. Vestibulum sit amet fringilla eros.</p>
  
      <p>Fusce consectetur sem urna, nec elementum quam pharetra ac. In non lobortis diam. Fusce sollicitudin tempus aliquam. Aenean iaculis, turpis sit amet gravida tincidunt, arcu sapien sagittis velit, at placerat erat massa ac lectus. Suspendisse nec mattis magna. Praesent velit erat, placerat a sapien nec, ultricies iaculis nisl. Aliquam dictum, sapien sit amet porttitor placerat, nisl erat congue justo, sed placerat urna mi a turpis.</p>
  
      <p>Proin sagittis dui a leo luctus accumsan at et neque. Ut aliquet lectus ut faucibus convallis. Pellentesque lectus neque, cursus et luctus id, vehicula eget dolor. Sed porta, lectus non tincidunt congue, lacus ligula rutrum ex, quis tempor ante mauris id quam. Etiam id aliquam nisi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus eu lacus in tortor dictum commodo ut ut metus. Aliquam fringilla nunc non tortor ullamcorper condimentum.</p>
    </div>
  </div>
</div>
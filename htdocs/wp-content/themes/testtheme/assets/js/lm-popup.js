;(function($, window, document) {
  $(function() {
    'use strict';
    if ( !$ ) { // if there's no jQuery, popup can't work
        return undefined;
    }

    $.fn.cfPopup = function(options) {

      var waitForFinalEvent = (function () {
        var timers = {};
        return function (callback, ms, uniqueId) {
          if (!uniqueId) {
            uniqueId = "Don't call this twice without a uniqueId";
          }
          if (timers[uniqueId]) {
            clearTimeout(timers[uniqueId]);
          }
          timers[uniqueId] = setTimeout(callback, ms);
        };
      })();     

      var settings = $.extend( {
        'popup'    : '.cf-popup-wrapper'
      }, options);

      var _this = this,
          $body = $('body'),
          $popup = $(settings.popup);

      _this.on('click', function(e){
        e.preventDefault();

        if (!$body.hasClass('cf-visible')) {
          $body.addClass('cf-visible');

          $popup.addClass('popup-visible');

          setPopupPosition();

        } 

      });

      var setPopupPosition = function() {

          var wind_h = $(window).height(),
              popup_h = $popup.height();

          var popup_top = (parseInt(wind_h, 10) - parseInt(popup_h, 10))/2;
          popup_top = (popup_top <= 40) ? 40 : popup_top;
          $popup.css({'top': Math.floor(popup_top + $(window).scrollTop() )});
      }

      $(window).on('resize', function () {
        waitForFinalEvent(function () {
          setPopupPosition();
        }, 100, 'setPopupPositionOnWindowResize');
      });       

      $('.cf-close, .cf-overlay').on('click', function(e){
        e.preventDefault();
        $body.removeClass('cf-visible');
        $popup.removeClass('popup-visible');
      });

    }


  });
}(window.jQuery, window, document));
<?php

add_action( 'wp_enqueue_scripts', 'lm_enqueue_styles' );
function lm_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

    wp_enqueue_style(  'twentyseventeen-style',  get_stylesheet_uri() );

  if ( is_rtl() ) {
    wp_enqueue_style(  'style-rtl',  get_stylesheet_directory_uri().'/style-rtl.css', array('twentyseventeen-style') );
  }

}




// Move the block “Add to Cart” above the description 
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 15 );


//Move the block with reviews under the description
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 35 );

//Delete the block “Category”
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);



//Changes on the page “Cart”. Change the names in the header of the table
/*

Я знаю, що можна було просто перезаписати шаблон cart.php створивши його копію в папці з child темою в /woocommerce/cart/cart.php
але цей спосіб мені здається елегантнішим, бо я таким чином не переписую шаблон і тепер не потрібно слідкувати за оновленями вукомерца у випадку, якщо вони щось змінять в шаблоні cart.php

*/
add_action('woocommerce_before_cart_table', 'lm_comment_table_header_start', 99999 );
function lm_comment_table_header_start() {
  echo '<!--';
}

add_action('woocommerce_before_cart_contents', 'lm_comment_table_header_end', 1 );
function lm_comment_table_header_end() {
  echo '-->'; 
?>
<table class="shop_table shop_table_responsive cart" cellspacing="0">
  <thead>
    <tr>
      <th class="product-remove">&nbsp;</th>
      <th class="product-thumbnail">&nbsp;</th>
      <th class="product-name"><?php _e( 'Product Name', 'ts-child' ); ?></th>
      <th class="product-price"><?php _e( 'Price', 'woocommerce' ); ?></th>
      <th class="product-quantity"><?php _e( 'Quantity', 'woocommerce' ); ?></th>
      <th class="product-subtotal"><?php _e( 'Total Price', 'ts-child' ); ?></th>
    </tr>
  </thead>
  <tbody>
<?php 
}



// Create cpt 'Books'
if ( ! function_exists('cpt_books') ) {

function cpt_books() {

  $labels = array(
    'name'                  => _x( 'Books', 'ts-child' ),
    'singular_name'         => _x( 'Book', 'ts-child' ),
    'menu_name'             => __( 'Books', 'ts-child' ),
    'name_admin_bar'        => __( 'Books', 'ts-child' ),
    'archives'              => __( 'Books Archives', 'ts-child' ),
    'attributes'            => __( 'Books Attributes', 'ts-child' ),
    'parent_item_colon'     => __( 'Parent Book:', 'ts-child' ),
    'all_items'             => __( 'All Books', 'ts-child' ),
    'add_new_item'          => __( 'Add New Book', 'ts-child' ),
    'add_new'               => __( 'Add New', 'ts-child' ),
    'new_item'              => __( 'New Book', 'ts-child' ),
    'edit_item'             => __( 'Edit Book', 'ts-child' ),
    'update_item'           => __( 'Update Book', 'ts-child' ),
    'view_item'             => __( 'View Book', 'ts-child' ),
    'view_items'            => __( 'View Book', 'ts-child' ),
    'search_items'          => __( 'Search Book', 'ts-child' ),
    'not_found'             => __( 'Not found', 'ts-child' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'ts-child' ),
    'featured_image'        => __( 'Featured Image', 'ts-child' ),
    'set_featured_image'    => __( 'Set featured image', 'ts-child' ),
    'remove_featured_image' => __( 'Remove featured image', 'ts-child' ),
    'use_featured_image'    => __( 'Use as featured image', 'ts-child' ),
    'insert_into_item'      => __( 'Insert into book', 'ts-child' ),
    'uploaded_to_this_item' => __( 'Uploaded to this book', 'ts-child' ),
    'items_list'            => __( 'Books list', 'ts-child' ),
    'items_list_navigation' => __( 'Books list navigation', 'ts-child' ),
    'filter_items_list'     => __( 'Filter books list', 'ts-child' ),
  );
  $args = array(
    'label'                 => __( 'Book', 'ts-child' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
    'taxonomies'            => array( 'genre' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'menu_icon'             => 'dashicons-book',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,    
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'post',
    'show_in_rest'          => true,
  );
  register_post_type( 'books', $args );

}
add_action( 'init', 'cpt_books', 0 );

}


// Register Genre Taxonomy for Books cpt
if ( ! function_exists( 'add_genre_taxonomy_to_cpt_books' ) ) {

function add_genre_taxonomy_to_cpt_books() {

  $labels = array(
    'name'                       => _x( 'Genres', 'ts-child' ),
    'singular_name'              => _x( 'Genre', 'ts-child' ),
    'menu_name'                  => __( 'Genres', 'ts-child' ),
    'all_items'                  => __( 'All Genres', 'ts-child' ),
    'parent_item'                => __( 'Parent Genre', 'ts-child' ),
    'parent_item_colon'          => __( 'Parent Genre:', 'ts-child' ),
    'new_item_name'              => __( 'New Genre Name', 'ts-child' ),
    'add_new_item'               => __( 'Add New Genre', 'ts-child' ),
    'edit_item'                  => __( 'Edit Genre', 'ts-child' ),
    'update_item'                => __( 'Update Genre', 'ts-child' ),
    'view_item'                  => __( 'View Genre', 'ts-child' ),
    'separate_items_with_commas' => __( 'Separate genres with commas', 'ts-child' ),
    'add_or_remove_items'        => __( 'Add or remove genres', 'ts-child' ),
    'choose_from_most_used'      => __( 'Choose from the most used', 'ts-child' ),
    'popular_items'              => __( 'Popular Genres', 'ts-child' ),
    'search_items'               => __( 'Search Genres', 'ts-child' ),
    'not_found'                  => __( 'Not Found', 'ts-child' ),
    'no_terms'                   => __( 'No genres', 'ts-child' ),
    'items_list'                 => __( 'Genres list', 'ts-child' ),
    'items_list_navigation'      => __( 'Genres list navigation', 'ts-child' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
    'show_in_rest'               => true,
  );
  register_taxonomy( 'genre', array( 'books' ), $args );

}
add_action( 'init', 'add_genre_taxonomy_to_cpt_books', 0 );

}


if ( is_rtl() ) {

  //add breadcrumb rtl support
  function woocommerce_breadcrumb( $args = array() ) {
    $args = wp_parse_args( $args, apply_filters( 'woocommerce_breadcrumb_defaults', array(
      'delimiter'   => '&nbsp;&#47;&nbsp;',
      'wrap_before' => '<nav class="woocommerce-breadcrumb" ' . ( is_single() ? 'itemprop="breadcrumb"' : '' ) . '>',
      'wrap_after'  => '</nav>',
      'before'      => '',
      'after'       => '',
      'home'        => _x( 'Home', 'breadcrumb', 'woocommerce' )
    ) ) );

    $breadcrumbs = new WC_Breadcrumb();

    if ( ! empty( $args['home'] ) ) {
      $breadcrumbs->add_crumb( $args['home'], apply_filters( 'woocommerce_breadcrumb_home_url', home_url() ) );
    }

    $args['breadcrumb'] = $breadcrumbs->generate();

    wc_get_template( 'global/breadcrumb-rtl.php', $args );
  }

  // fix brackets 
  add_filter('the_title', 'lm_rtl_title_fix', 100, 2 );
  function lm_rtl_title_fix($title, $id) {
    return $title."&lrm;";
  }

}

require_once('lm-popup-functions.php');

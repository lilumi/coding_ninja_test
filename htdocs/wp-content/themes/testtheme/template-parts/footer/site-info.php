<div class="footer-cta">
  <a class="phone" href="tel:+380987654321">Call Us<span>: 0987654321</span></a>
  <a class="email" href="mailto:testdomain@mail.to">Email<span class="visible-mobile"> Us</span><span>: testdomain@mail.to</span></a>
  <a class="contact-form btn" href="#">Contact Us</a>
</div>

<div class="site-info">
  <a href="<?php echo esc_url( __( 'https://wordpress.org/', 'twentyseventeen' ) ); ?>"><?php printf( __( 'Proudly powered by %s', 'twentyseventeen' ), 'WordPress' ); ?></a>
</div><!-- .site-info -->